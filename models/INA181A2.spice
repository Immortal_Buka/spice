.SUBCKT INA181A2 INP INN VCC GND REF OUT
E_E2         N609752 0 GND 0 -1
X_U40         VCC GND INPUT_VCLAMP INPUT_ZOUT VIMON EN GNDF
+  VOUTVSIOUT_NROS_SHDN 
E_E11         INP_BUF_OUT GNDF INP GNDF 1
X_U12         INPUT_TF INPUT_VCLAMP VCC GND EN GNDF TF
X_U39         OUT_CNTRL VICM VCC GND GNDF CONTROL 
C_C3         N337909 0  1  TC=0,0 
E_E12         INN_BUF_OUT GNDF N409527 GNDF 1
X_U38         VCC GND INP_PSRR_IN INP_PSRR_OUT GNDF PSRR 
R_R23         OUT_CNTRL N698091  1m TC=0,0 
X_U31         INPUT_VIMON OUT VIMON GNDF AMETER 
R_R19         INPUT_OUTP REF  500k TC=0,0 
X_U18         INP GNDF VCC GND VICM GNDF IIBP
D_D1         IN_COMP N698091 Dideal 
R_R22         OUT_CNTRL IN_COMP  1000 TC=0,0 
R_R11         INPUT_ZOUT INPUT_VIMON  10 TC=0,0 
X_U5         VICM INP INN GNDF VICM
X_U37         INPUTP_GBW INPUT_OUTP vnse 
E_E13         INP_BUF_OUT INP_CMRR VICM GNDF 0
R_R16         GNDF INP  1e9 TC=0,0 
V_V9         COMP_REF GNDF 0.7Vdc
E_E14         INN_BUF_OUT INN_CMRR VICM GNDF 0
V_V8         INN N409527 -12.5uVdc
C_C2         N337676 0  1  TC=0,0 
X_U17         INN GNDF VCC GND VICM GNDF IIBN
R_R4         INN_CMRR INPUTN_GBW  10k TC=0,0 
X_U36         VICM INP_CMRR INP_PSRR_IN GNDF CMRR 
R_R18         OUT INPUTN_GBW  500k TC=0,0 
X_U43         VCC GND VICM EN VIMON GNDF IQ_VICM 
R_R21         N609752 N337909  1 TC=0,0 
R_R3         INP_PSRR_OUT INPUT_OUTP  10k TC=0,0 
C_C1         N337909 N337676  1  TC=0,0 
X_U42         EN IN_COMP COMP_REF GNDF COMPARATOR 
E_E3         GNDF 0 N337676 N337909 0.5
R_R17         INN GNDF  1e9 TC=0,0 
R_R20         N609492 N337676  1 TC=0,0 
C_C4         GNDF IN_COMP  3n  TC=0,0 
E_E1         N609492 0 VCC 0 1
R_R24         GNDF EN  1k TC=0,0 
X_U41         INPUTP_GBW INPUTN_GBW INPUT_TF EN VCC GND GNDF GBW_SLEW_OUTLIMITS
*.MODEL Dideal D Rs = 0.001 N = 0.001
.MODEL Dideal D 
.ENDS
*$
************
*          *
*          *
************
.SUBCKT VNSE 1 2 
.PARAM NLF = 35.25 
.PARAM FLW = 1u  
.PARAM NVR = 35.25
* BEGIN SETUP OF NOISE GEN - NANOVOLT/RT-HZ
* INPUT THREE VARIABLES
* SET UP VNSE 1/F
********************
* NV/RHZ AT 1/F FREQ
* NLF
********************
* FREQ FOR 1/F VAL
* FLW
********************
* SET UP VNSE FB
* NV/RHZ FLATBAND
* NVR
********************
* END USER INPUT
* START CALC VALS
.PARAM GLF={PWR(FLW,0.25)*NLF/1164}
.PARAM RNV={1.184*PWR(NVR,2)}
.MODEL DVN D KF={PWR(FLW,0.5)/1E11} IS=1.0E-16
* END CALC VALS
I1 0 7 10E-3
I2 0 8 10E-3
D1 7 0 DVN
D2 8 0 DVN
E1 3 6 7 8 {GLF}
R1 3 0 1E9
R2 3 0 1E9
R3 3 6 1E9
E2 6 4 5 0 10
R4 5 0 {RNV}
R5 5 0 {RNV}
R6 3 4 1E9
R7 4 0 1E9
E3 1 2 3 4 1
C1 1 0 1E-15
C2 2 0 1E-15
C3 1 2 1E-15
.ENDS
************
*          *
*          *
************
*$
.SUBCKT COMPARATOR OUT IN REF GNDF
.PARAM VOUT_MAX = 1
.PARAM VOUT_MIN = 0
.PARAM GAIN = 1e4
EOUT OUT GNDF VALUE = {MAX(MIN(GAIN*V(IN,REF),VOUT_MAX),VOUT_MIN)}
.ENDS 
*$
************
*          *
*          *
************
.SUBCKT IQ_VICM VCC VEE VICM SHDN VIMON GNDF
***
* params for IQ vs VICM
.PARAM IQ_3 = 209u
.PARAM IQ_2 = 325u
.PARAM IQ_1 = 375u
.PARAM VICM_1 = -0.2
.PARAM VICM_2 = 2
.PARAM VICM_3 = 5.1
.PARAM m_1 = {-2.25*12.4u}
.PARAM b_1 = 370u
.PARAM m_2 = 0
.PARAM b_2 = 313.3u
.PARAM m_3 = -1.04u
.PARAM b_3 = 208.8u
***
.PARAM IQ_SHDN = 0.01u
.PARAM Geq = 18.75u
* Assumes: V(SHDN) = 1 when device is enabled
*          V(SHDN) = 0 when device is disabled
*** IQ varying w/supply, negligible in shutdown ***
*GVAR VCC VEE VALUE = {(V(SHDN)+ 1e-9)*Geq}
*** Nominal IQ       ---Enabled---     ---Shutdown----
*GIQ VCC VEE VALUE = {IQ_NOM*V(SHDN,GNDF)}
***
* IQ variations with VICM
* If VICM is > VICM_XX then = +1, else = -1
EZ1 NSGNVICM1 0 VALUE = {SGN(V(VICM) - VICM_1) + ABS(SGN(V(VICM) - VICM_1)) - 1}
EZ2 NSGNVICM2 0 VALUE = {SGN(V(VICM) - VICM_2) + ABS(SGN(V(VICM) - VICM_2)) - 1}
EZ3 NSGNVICM3 0 VALUE = {SGN(V(VICM) - VICM_3) + ABS(SGN(V(VICM) - VICM_3)) - 1}
EIQ1 NIQ1 0 VALUE = {m_1*V(VICM)+b_1}
EIQ2 NIQ2 0 VALUE = {m_2*V(VICM)+b_2}
EIQ3 NIQ3 0 VALUE = {m_3*V(VICM)+b_3}
EIQ1_ACT NIQ1_ACT 0 VALUE = {0.5*(1 - V(NSGNVICM2))*V(NIQ1)}
EIQ2_ACT NIQ2_ACT 0 VALUE = {0.5*(1 + V(NSGNVICM2))*0.5*(1 - V(NSGNVICM3))*V(NIQ2)}
EIQ3_ACT NIQ3_ACT 0 VALUE = {0.5*(1 + V(NSGNVICM3))*V(NIQ3)}
GIQ VCC VEE VALUE = {(V(NIQ1_ACT) + V(NIQ2_ACT) + V(NIQ3_ACT))*V(SHDN,GNDF)}
***
* Test for sign of output current:
ESGNIOUT NSGNIOUT 0 VALUE = {SGN(V(VIMON,GNDF))}
* Add output current to VCC or GND current based on sign
* and magnitude of output current
GOUTP VCC GNDF VALUE = {0.5*(1 + V(NSGNIOUT))*V(VIMON,GNDF)*V(SHDN,GNDF)}
GOUTN GNDF VEE VALUE = {0.5*(1 - V(NSGNIOUT))*V(VIMON,GNDF)*V(SHDN,GNDF)}
.ENDS
*$
************
*          *
*          *
************
.SUBCKT CMRR  VICM VI  VO  GNDF 
.PARAM CMRR = 119 
.PARAM fcmrr = 5k
.PARAM PI = 3.141592
.PARAM RCMRR = 1
.PARAM GCMRR = {PWR(10,-CMRR/20)/RCMRR}
.PARAM LCMRR = {RCMRR/(2*PI*fcmrr)}
.PARAM CCMRR = {RCMRR/(2*PI*fcmrr)}
G1  GNDF 1 VICM GNDF {GCMRR}
R1  1 2 {RCMRR}
L1  2 GNDF {LCMRR}
E1  VI VO 1 GNDF 1
.ENDS
*$
************
*          *
*          *
************
.SUBCKT IIBN OUT IN VCC VEE INN GNDF
*** OUT and IN are for IIB current flow **
*** INP is for voltage monitoring of the amp inverting 
*** input or for VICM 
****
**** IIB shift vs VIN from data sheet curves ***
*.PARAM m1v = {2*53u/30}
*.PARAM m2v = 1.1u
**** IIB vs VIN linear intercepts from data sheet curves ***
*.PARAM b1v = 0
*.PARAM b2v = 54u
****** VCM Breakpoint between two curves
.PARAM VCMBRK = 5
******
****** Equivalent resistances dependent on VCC
.PARAM REQ_VCC_NEZERO = 450k
.PARAM REQ_VCC_EQZERO = 530k
******
****** Test for VCC-VEE > 0, =1+1, = -1 otherwise
EVCCTEST NVCCTEST 0 VALUE = {SGN(V(VCC,VEE)) - 1 + ABS(SGN(V(VCC,VEE)))}
******
****** Negative sign if V(INN) <= VCMBRK
****** Positiive sign if V(INN) > VCMBRK
E1ACT N1SGN 0 VALUE = {SGN(V(INN)-VCMBRK) - 1 + ABS(SGN(V(INN)-VCMBRK))}
****
**** Two lines w/IIB vs input voltage ****
*E1v N1v 0 VALUE = {m1v*V(INN) + b1v}
*E2v N2v 0 VALUE = {m2v*V(INN) + b2v}
**** Select one of two IIB lines
*EIIB NIIBV 0 VALUE = {0.5*(1-V(N1SGN))*V(N1v) + 0.5*(1+V(N1SGN))*V(N2v)}
****
**** Adjust resistor load on V(IN) depdendent upon VCC > 0
GOUT2 OUT IN VALUE = {V(INN,GNDF)*(1/REQ_VCC_NEZERO 
+ + 0.5*(1-V(NVCCTEST ))/REQ_VCC_EQZERO)
+ + 0.5*(1-V(NVCCTEST ))*(-5u)}
**
*GOUT OUT IN VALUE = {(V(NIIBV))}
GOUT1 OUT IN VALUE = {0.5*(1+V(N1SGN))*0.5*(1+V(NVCCTEST))*60u}
.ENDS
*$
************
*          *
*          *
************
.SUBCKT VICM OUT INP INN GNDF
EOUT OUT GNDF VALUE = {0.5*(V(INP,GNDF) + V(INN,GNDF))}
.ENDS
*$
************
*          *
*          *
************
.SUBCKT IIBP OUT IN VCC VEE INP GNDF
*** OUT and IN are for IIB current flow **
*** INP is for voltage monitoring of the amp inverting 
*** input or for VICM 
****
**** IIB shift vs VIN from data sheet curves ***
*.PARAM m1v = {2*53u/30}
*.PARAM m2v = 1.1u
**** IIB vs VIN linear intercepts from data sheet curves ***
*.PARAM b1v = 0
*.PARAM b2v = 54u
****** VCM Breakpoint between two curves
.PARAM VCMBRK = 5
******
****** Equivalent resistances dependent on VCC
.PARAM REQ_VCC_NEZERO = 450k
.PARAM REQ_VCC_EQZERO = 530k
******
****** Test for VCC-VEE > 0, =1+1, = -1 otherwise
EVCCTEST NVCCTEST 0 VALUE = {SGN(V(VCC,VEE)) - 1 + ABS(SGN(V(VCC,VEE)))}
******
****** Negative sign if V(INP) <= VCMBRK
****** Positiive sign if V(INP) > VCMBRK
E1ACT N1SGN 0 VALUE = {SGN(V(INP)-VCMBRK) - 1 + ABS(SGN(V(INP)-VCMBRK))}
****
**** Two lines w/IIB vs input voltage ****
*E1v N1v 0 VALUE = {m1v*V(INP) + b1v}
*E2v N2v 0 VALUE = {m2v*V(INP) + b2v}
**** Select one of two IIB lines
*EIIB NIIBV 0 VALUE = {0.5*(1-V(N1SGN))*V(N1v) + 0.5*(1+V(N1SGN))*V(N2v)}
****
**** Adjust resistor load on V(IN) depdendent upon VCC > 0
GOUT2 OUT IN VALUE = {V(INP,GNDF)*(1/REQ_VCC_NEZERO 
+ + 0.5*(1-V(NVCCTEST ))/REQ_VCC_EQZERO)
+ + 0.5*(1-V(NVCCTEST ))*(-5u)}
**
*GOUT OUT IN VALUE = {(V(NIIBV))}
GOUT1 OUT IN VALUE = {0.5*(1+V(N1SGN))*0.5*(1+V(NVCCTEST))*60u}
.ENDS
*$
************
*          *
*          *
************
.SUBCKT AMETER  VI  VO VIMON GNDF
.PARAM GAIN = 1
VSENSE VI VO DC = 0
EMETER VIMON GNDF VALUE = {I(VSENSE)*GAIN}
.ENDS
*$
************
*          *
*          *
************
.SUBCKT PSRR  VDD  VSS  VI  VO  GNDF 
.PARAM PSRR = 105
.PARAM fpsrr = 1300
.PARAM PI = 3.141592
.PARAM RPSRR = 1
.PARAM GPSRR = {PWR(10,-PSRR/20)/RPSRR}
.PARAM LPSRR = {RPSRR/(2*PI*fpsrr)}
G1  GNDF 1 VDD VSS {GPSRR}
R1  1 2 {RPSRR}
L1  2 GNDF {LPSRR} 
E1  VO VI 1 GNDF 1
*C2  VDD VSS 10P 
.ENDS
*$
************
*          *
*          *
************
.SUBCKT CONTROL OUT_CNTRL IN VCC VEE GNDF
* Test for power supply within range
* Test for VICM within range
* Disable device if either are out of range.
.PARAM VCCMAX = 6.0
.PARAM VCCMIN = 2.69
.PARAM VICMMAX = 26.01
.PARAM VICMMIN = -0.199
**
*** Test power supplies. 
EN1 N1 0 VALUE = {SGN(VCCMAX - V(VCC,VEE)) - 1 + ABS(SGN(VCCMAX - V(VCC,VEE)))}
EN2 N2 0 VALUE = {SGN(V(VCC,VEE)-VCCMIN) - 1 + ABS(SGN(V(VCC,VEE)-VCCMIN))}
EVCCMAX NVCCMAX 0 VALUE = {0.5*(1 + V(N1))}
EVCCMIN NVCCMIN 0 VALUE = {0.5*(1 + V(N2))}
*** Test VICM. Will return TRUE if within limits
EN3 N3 0 VALUE = {SGN(VICMMAX - V(IN)) - 1 + ABS(SGN(VICMMAX - V(IN)))}
EN4 N4 0 VALUE = {SGN(V(IN)-VICMMIN) - 1 + ABS(SGN(V(IN)-VICMMIN))}
EVICMMAX NVICMMAX 0 VALUE = {0.5*(1 + V(N3))}
EVICMMIN NVICMMIN 0 VALUE = {0.5*(1 + V(N4))}
**
* AND the four conditions
EOUT OUT_CNTRL GNDF VALUE = {V(NVCCMAX)*V(NVCCMIN)*V(NVICMMAX)*V(NVICMMIN)}
.ENDS
*$
************
*          *
*          *
************
.SUBCKT TF  VI  VO  VCC VEE SHDN GNDF
.PARAM fz1 = 10G 
.PARAM fz2 = 10G 
.PARAM fz3 = 10G 
.PARAM fz4 = 10G 
.PARAM fz5 = 10G
*.PARAM fp1 = 45e6 
.PARAM fp1 = 10e6 
.PARAM fp2 = 10G 
.PARAM fp3 = 10G 
.PARAM fp4 = 10G
.PARAM Gm = 1M
.PARAM Ro = {1/Gm}
.PARAM PI = 3.141592
.PARAM gL = 1M
Gp1  GNDF Vp1 VI GNDF {Gm}
Rp1  Vp1 GNDF {Ro}
Cp1  Vp1 GNDF {1/(2*PI*Ro*fp1)} IC = 0
Gp2  GNDF Vp2 Vp1 GNDF {Gm}
Rp2  Vp2 GNDF {Ro}
Cp2  Vp2 GNDF {1/(2*PI*Ro*fp2)} IC = 0
Gp3  GNDF Vp3 Vp2 GNDF {Gm}
Rp3  Vp3 GNDF {Ro}
Cp3  Vp3 GNDF {1/(2*PI*Ro*fp3)} IC = 0
Gp4  GNDF VO Vp3 GNDF {Gm}
Rp4  VO GNDF {Ro}
Cp4  VO GNDF {1/(2*PI*Ro*fp4)} IC = 0
*Gz1  GNDF Vz1 Vp4 GNDF {Gm}
*Rz1  Vz1 Vx1  {Ro}
*G11  Vy1 GNDF Vx1 GNDF {gL}
*G12  Vx1 GNDF Vy1 GNDF {gL}
*Cz1  Vy1 GNDF {(Ro*gL**2)/(2*PI*fz1)} IC = 0
*R11  Vy1 GNDF 1E9
*Gz2  GNDF Vz2 Vz1 GNDF {Gm}
*Rz2  Vz2 Vx2  {Ro}
*G21  Vy2 GNDF Vx2 GNDF {gL}
*G22  Vx2 GNDF Vy2 GNDF {gL}
*Cz2  Vy2 GNDF {(Ro*gL**2)/(2*PI*fz2)} IC = 0
*R22  Vy2 GNDF 1E9
*Gz3  GNDF Vz3 Vz2 GNDF {Gm}
*Rz3  Vz3 Vx3  {Ro}
*G31  Vy3 GNDF Vx3 GNDF {gL}
*G32  Vx3 GNDF Vy3 GNDF {gL}
*Cz3  Vy3 GNDF {(Ro*gL**2)/(2*PI*fz3)} IC = 0
*R33  Vy3 GNDF 1E9
*Gz4  GNDF Vz4 Vz3 GNDF {Gm}
*Rz4  Vz4 Vx4  {Ro}
*G41  Vy4 GNDF Vx4 GNDF {gL}
*G42  Vx4 GNDF Vy4 GNDF {gL}
*Cz4  Vy4 GNDF {(Ro*gL**2)/(2*PI*fz4)} IC = 0
*R44  Vy4 GNDF 1E9
*Gz5  GNDF VO Vz4 GNDF {Gm}
*Rz5  VO Vx5  {Ro}
*G51  Vy5 GNDF Vx5 GNDF {gL}
*G52  Vx5 GNDF Vy5 GNDF {gL}
*Cz5  Vy5 GNDF {(Ro*gL**2)/(2*PI*fz4)} IC = 0
*R55  Vy5 GNDF 1E9
.ENDS
*$
************
*          *
*          *
************
.SUBCKT VOUTvsIOUT_NROS_SHDN  VCC  VEE  VI  VO VIMON SHDN GNDF
.PARAM ISC = 0.020
* MAX linear current
.PARAM IMAXLIN = 0.016
* Output voltage at MAX linear current
.PARAM VOUTMAXLIN = 4.3
* Output voltage at IOUT = 0
.PARAM VHR0 = 0.02
* VCC used in data sheet Testing
.PARAM VCC_Test = 5.5
**
*** Calc MAX/MIN VOUT for IOUT < IMAXLIN ***
* Negative slope of VOUT/IOUT curve for IOUT < IMAXLIN
ER1 NR1 GNDF VALUE = {((VCC_Test - VHR0)  - VOUTMAXLIN)/IMAXLIN}
* MAX/MIN VOUT for IOUT < IMAXLIN
* V(VCC,GNDF) - VHR0 is the y-intercept: MAX VOUT for IOUT = 0
EVOUT1pos NVOUT1_MAX GNDF VALUE = {V(VCC,GNDF) - VHR0 - V(NR1,GNDF)*V(VIMON,GNDF)}
EVOUT1neg NVOUT1_MIN GNDF VALUE = {V(VEE,GNDF) + VHR0 - V(NR1,GNDF)*V(VIMON,GNDF)}
********************************************
* 
*** Calc MAX/MIN VOUT for IMAXLIN < IOUT < ISC ***
* V(NV2,GNDF) is the the theoretical MAX VOUT for IMAXLIN < IOUT < ISC
* For the VOUT vs IOUT curve, it is the y-intercept for  this part of the curve
EV2 NV2 GNDF VALUE = {(V(VCC,GNDF) - VHR0 - V(NR1,GNDF)*IMAXLIN)/(1 - IMAXLIN/ISC)}
* Negative slope of VOUT/IOUT curve for IMAXLIN < IOUT < ISC
ER2 NR2 GNDF VALUE = {V(NV2,GNDF)/ISC}
EVOUT2pos NVOUT2_MAX GNDF VALUE = {V(NV2,GNDF) - V(VIMON,GNDF)*V(NR2,GNDF)}
EVOUT2neg NVOUT2_MIN GNDF VALUE = {-V(NV2,GNDF) - V(VIMON,GNDF)*V(NR2,GNDF)}
*********************************************
*
*** Choose MAX/MIN VOUT based on IOUT (VIMON) ***
*** Pick one of the two asymptotes cac's above
EHRPOS HRPOS GNDF VALUE = {MIN(V(NVOUT1_MAX,GNDF),V(NVOUT2_MAX,GNDF))}
EHRNEG HRNEG GNDF VALUE = {MAX(V(NVOUT1_MIN,GNDF),V(NVOUT2_MIN,GNDF))}
*************************************************
*
*** Clamp output if req'd
ECLAMP  VO GNDF VALUE = {V(SHDN,GNDF)*MAX(MIN(V(VI,GNDF), V(HRPOS,GNDF)), V(HRNEG,GNDF))}
**
*
.ENDS
*$
************
*          *
*          *
************
.SUBCKT GBW_SLEW_OUTLIMITS  VIP  VIM  VO SHDN VCC VEE GNDF 
.PARAM Aol = 160  
.PARAM GBW = 10.5e6  
.PARAM SRP = 5e6  
.PARAM SRN = 5e6 
.PARAM IT = 0.001
.PARAM PI = 3.141592
.PARAM IP = {IT*MAX(1,SRP/SRN)}
.PARAM IN = {IT*MIN(-1,-SRN/SRP)}
.PARAM CC = {IT*MAX(1/SRP,1/SRN)}
.PARAM FP = {GBW/PWR(10,AOL/20)}
.PARAM RC = {1/(2*PI*CC*FP)}
.PARAM GC = {PWR(10,AOL/20)/RC}
* Loading the VO pin with an external resistor will change the AOL.
G1p GNDF OUTG1p VALUE = {MAX(MIN(GC*V(SHDN,GNDF)*V(VIP,VIM),IP),IN)}
G1n OUTG1n GNDF VALUE = {MAX(MIN(GC*V(SHDN,GNDF)*V(VIP,VIM),IP),IN)}
E1OUT VO GNDF VALUE = {MAX(MIN(V(SHDN,GNDF)*V(OUTG1p,OUTG1n),V(VCC,GNDF)),V(VEE,GNDF))}
*G1OUT GNDF VO VALUE = {V(SHDN,GNDF)*V(OUTG1p,OUTG1n)}
*G1OUT GNDF VO VALUE = {MAX(MIN(V(SHDN,GNDF)*V(OUTG1p,OUTG1n),10),-10)}
*G1OUT GNDF VO OUTG1p OUTG1n 1
RG1p OUTG1p GNDF {0.5*RC}
Cg1dp OUTG1p GNDF {2*CC} IC=0
RG1n OUTG1n GNDF {0.5*RC}
Cg1dn OUTG1n GNDF {2*CC} IC=0
*ROUT VO GNDF 1
*G1 GNDF OUTG1 VALUE = {MAX(MIN(GC*V(VIP,VIM),IP),IN)}
*G1OUT GNDF VO VALUE = {V(SHDN,GNDF)*V(OUTG1,GNDF)}
*C1 OUTG1 GNDF {CC} IC=0
*R1 OUTG1 GNDF {RC}
*ROUT VO GNDF 1
.ENDS
*$

